module FMarkdown.Tests

open FMarkdown
open NUnit.Framework

// Tests
type TestDatum<'Input, 'Output> = { Input: 'Input; Exp: 'Output }


type public Tests () =
    member this.runTests testData f =
        testData
        |> List.iter (fun testDatum ->
            let act = f testDatum.Input
            Assert.AreEqual (testDatum.Exp, act))

    member this.parserTestData =
        [ { Input = "# hej med dig\n\njeg hedder kaj"
            Exp = "<h1>hej med dig</h1><p>jeg hedder kaj</p>" }

          { Input = "# her er noget `mere` komplekst"
            Exp = "<h1>her er noget <code>mere</code> komplekst</h1>" }

          { Input = "## her er noget `mere` komplekst"
            Exp = "<h2>her er noget <code>mere</code> komplekst</h2>" }

          { Input = "# her er noget **`mere`** komplekst"
            Exp = "<h1>her er noget <b><code>mere</code></b> komplekst</h1>" }

          { Input = "####### her er noget **`mere`** komplekst"
            Exp = "<p>####### her er noget <b><code>mere</code></b> komplekst</p>" }

          { Input = "her er noget simpelt"
            Exp = "<p>her er noget simpelt</p>" }

          { Input = "her er *nogle* *italics*"
            Exp = "<p>her er <i>nogle</i> <i>italics</i></p>" }

          { Input = "some *italics \* still italics*"
            Exp = "<p>some <i>italics * still italics</i></p>" }

          { Input = @"Escaped \\ backslash"
            Exp = @"<p>Escaped \ backslash</p>" }

          { Input = "# her er noget **`*mere*`** komplekst"
            Exp = "<h1>her er noget <b><code>*mere*</code></b> komplekst</h1>" }

          { Input = "```fsharp\nlet x = 2\n```"
            Exp = "<pre class=\"lang-fsharp\">let x = 2</pre>" }

          { Input = "```\nlet x = 2\n```"
            Exp = "<pre>let x = 2</pre>" }

          { Input = "```**\nlet x = 2```"
            Exp = "<pre class=\"lang-**\">let x = 2```</pre>" }

          { Input = "```fsharp\nlet x = y * x\n```"
            Exp = "<pre class=\"lang-fsharp\">let x = y * x</pre>" }

          { Input = "```fsharp\n```"
            Exp = "<pre class=\"lang-fsharp\"></pre>" }

          { Input = "```\n```"
            Exp = "<pre></pre>" }

          { Input =
              @"# My first article

I expect this blog to be fun and *inspirational.* (Yes, I added the **emphasis.**)

Something I would *never* do is add both *bold **and** emphasis* to a string.

Speaking of `strings...` When you call methods on types, `do you expect *this?*`

```markdown
I certainly don't!
```

And neither do I expect this!"
            Exp =
              "<h1>My first article</h1><p>I expect this blog to be fun and <i>inspirational.</i> (Yes, I added the <b>emphasis.</b>)</p><p>Something I would <i>never</i> do is add both <i>bold <b>and</b> emphasis</i> to a string.</p><p>Speaking of <code>strings...</code> When you call methods on types, <code>do you expect *this?*</code></p><pre class=\"lang-markdown\">I certainly don't!</pre><p>And neither do I expect this!</p>" }

          { Input =
              @"### I `mean...`

Is there even any *reason* you shouldn't be able to have your cake and eat it too?

```fsharp
```

```fsharp
match x with
| 2 -> 3
*DDD
```

```pitchfork
is a controversial site
```"
            Exp =
              "<h3>I <code>mean...</code></h3><p>Is there even any <i>reason</i> you shouldn't be able to have your cake and eat it too?</p><pre class=\"lang-fsharp\"></pre><pre class=\"lang-fsharp\">match x with\n| 2 -> 3\n*DDD</pre><pre class=\"lang-pitchfork\">is a controversial site</pre>" }

          { Input =
              """### I `mean...`

<div class="container">hej med <p>dig</p></div>

```pitchfork
is a controversial site
```"""
            Exp =
              "<h3>I <code>mean...</code></h3><div class=\"container\">hej med <p>dig</p></div><pre class=\"lang-pitchfork\">is a controversial site</pre>" } ]

    member this.stringExtensionsTestData =
        [ { Input = "hej med dig\njeg hedder kaj"
            Exp = "hej med dig" } ]

    member this.stringTakeUntilTestData =
        [ { Input = "hej med dig\n```jeg hedder kaj"
            Exp = "hej med dig" } ]

    [<Test>]
    member this.test_parseStrings () = this.runTests this.parserTestData parse

    [<Test>]
    member this.test_stringTakeWhile () =
        this.runTests this.stringExtensionsTestData (String.takeWhile (fun c -> c <> '\n'))

    [<Test>]
    member this.test_stringTakeUntil () =
        this.runTests this.stringTakeUntilTestData (String.takeUntil "\n```")
